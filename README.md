# Architecture projet 

##Dossiers
 - Assets : Contient les images
 - Store : Contient tous les informations(action,type,reducer) sur les éléments Briefs et Product 
 - Style : Tout les composants de design(circle progress, menu,etc.)
##Fichiers
- BrefHistory : Affiche les briefs
- BrefInterface : Formulaire permettant l'ajout d'un briefs
- thunk : Regroupe toute les requetes asynchrone.
- App.tsx : Composant principal 
 
##Information complémentaire
Inspiré : https://codesandbox.io/s/w02m7jm3q7
