import React from 'react';
import './App.css';
import {sendBref} from "./store/bref/actions";
import {BrefState, Bref} from "./store/bref/types";
import {connect} from "react-redux";
import {AppState} from "./store";
import BrefHistory from "./BrefHistory";
import BrefInterface from "./BrefInterface";
import {thunkAddBrefApi, thunkChargeBref, thunkChargeProduct, thunkFilterBrefApi} from "./thunks";
import {sendProduct} from "./store/product/actions";
import {Product, ProductState} from "./store/product/types";
import DenseAppBar from "./Style/BarMenu";
import CircleProgress from "./Style/CircleProgress";
import {createSelector} from "reselect";

interface AppProps {
  sendBref: typeof sendBref;
  sendProduct: typeof sendProduct;
  bref: BrefState;
  product: ProductState;
  thunkChargeBref: any;
  thunkChargeProduct: any;
  thunkAddBrefApi: any;
  thunkFilterBrefApi: any;
}


export type UpdateBrefComment = React.SyntheticEvent<{ value: string }>;
export type UpdateBrefTitle = React.SyntheticEvent<{ value: string }>;
export type updateBrefProduct = React.ChangeEvent<{ value: unknown }>;
export type updateFilterBrefProduct = React.ChangeEvent<{ value: unknown }>;

// const App: React.FC<AppProps> = (props: AppProps) => {

class App extends React.Component<AppProps> {
  state = {
    title: "",
    comment: "",
    product: {},
    indexProduct: 0,
    classes: '',
    indexFilterProduct: -1,
  };
  /*
      Data recovery when initializing the page
  -------------------------------------------------------------
   */
  componentDidMount() {
  this.props.thunkChargeBref();
  this.props.thunkChargeProduct();
  }
// -------------------------------------------------------------

  /*
    Form event management
    -----------------------------------------------------------------------------
   */
  updateBrefTitle = (event: UpdateBrefTitle) => {
    this.setState({ title: event.currentTarget.value });
  };
    updateBrefComment = (event: UpdateBrefComment) => {
    this.setState({ comment: event.currentTarget.value });
  };
  updateBrefProduct = (event: updateBrefProduct) => {
    this.setState({ indexProduct: Number(event.target.value as string) });
  };

  sendBref =  (title: string,comment: string, product: Product) => {
    this.props.thunkAddBrefApi({
      title: title,
      comment: comment,
      product: product
    });
    this.setState({ comment: "" });
    this.setState({ title: "" });
    this.setState({ product: {} });
    this.setState({ indexProduct: 0 });
  };
  //----------------------------------------------------------------------------

  /*
      Event Filter on List Briefs
   -------------------------------------------------------------------------------
   */
  updateFilterBrefProduct = (event: updateFilterBrefProduct) => {
    this.setState({ indexFilterProduct: Number(event.target.value as string) });
    var recupValue=event.target.value as string;
    if(recupValue === '-1'){
      recupValue= 'SHOW_ALL';
    }
    this.props.thunkFilterBrefApi(recupValue);
  };
  // -------------------------------------------------------------------------------
  render() {
    return (
        <div className="App">
          <DenseAppBar/>
          {this.props.bref.error.length>0 && <h3 className="txtCenter">{this.props.bref.error}</h3>}
          {this.props.bref.succes.length>0 && <h3 className="txtCenter">{this.props.bref.succes}</h3>}
          {this.props.product.error.length>0 && <h3 className="txtCenter">{this.props.product.error}</h3>}
          {this.props.product.succes.length>0 && <h3 className="txtCenter">{this.props.product.succes}</h3>}
          <div className="global">
          <div className="parent">

            {this.props.product.products.length>0 &&
                  <BrefInterface
                  title={this.state.title}
                  comment={this.state.comment}
                  updateBrefTitle={this.updateBrefTitle}
                  updateBrefComment={this.updateBrefComment}
                  updateBrefProduct={this.updateBrefProduct}
                  sendBref={this.sendBref}
                  indexProduct={this.state.indexProduct}
                  products={this.props.product.products}
              />
            }

          </div>
          {(this.props.bref.loading || this.props.product.loading) && <CircleProgress/>}
          {this.props.bref.Brefs.length>=0 && <BrefHistory brefs={this.props.bref.Brefs} updateFilterBrefProduct={this.updateFilterBrefProduct} indexFilterProduct={this.state.indexFilterProduct} products={this.props.product.products}  />}
        </div>
        </div>
    );
  };
}

/*
*------------------------------------------------------------------------------------
* Constant for filtering breifs and displaying the product name
* ------------------------------------------------------------------------------
* */
const lesbrefs = (brefStates: BrefState) => brefStates.Brefs;
const lesproducts = (state: AppState) => state.product.products;
const filters = (brefStates: BrefState) => Number(brefStates.filter);
const idProduct = (state: AppState,i:number) => state.bref.Brefs[i].product.id;

const SelectorVisibleToBrefProductName =createSelector( [lesproducts,idProduct],(products,id) => {return products.find(product => product.id === id)});
const SelectorVisibleToBref =createSelector<BrefState,Bref[],number,Bref[]>( [lesbrefs,filters],(brefs,indexFilterProduct) => {return brefs.filter(bref => bref.product.id === indexFilterProduct )});
const getVisibleToBrefs = (state: AppState) => {
  for(let i=0;i<state.bref.Brefs.length;i++){
   let recup = SelectorVisibleToBrefProductName(state,i);
   if(recup !== undefined){
     state.bref.Brefs[i].product=recup;
   }
  }
  if(state.bref.filter === 'SHOW_ALL'){
    return state.bref;
  }else if(Number(state.bref.filter)>=1){
    return {
      Brefs: SelectorVisibleToBref(state.bref),
      loading: state.bref.loading,
      error: state.bref.error,
      succes: state.bref.succes,
      filter: state.bref.filter
    }
  }else{
    return  state.bref;
  }
  };

//-------------------------------------------------------------------------------

const mapStateToProps = (state: AppState) => ({
  bref: getVisibleToBrefs(state),
  product: state.product,
});

export default connect(
    mapStateToProps,
    { sendBref, thunkChargeBref ,sendProduct, thunkChargeProduct, thunkAddBrefApi,thunkFilterBrefApi})(App);


