import * as React from "react";
import {Bref} from "./store/bref/types";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { updateFilterBrefProduct } from "./App";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {BootstrapInput, useStyles} from "./Style/BrefDesign";
import {Product} from "./store/product/types";

interface BrefHistoryProps {
    brefs: Bref[];
    updateFilterBrefProduct: (event: updateFilterBrefProduct) => void;
    indexFilterProduct: number;
    products: Product[];
}

const BrefHistory: React.FC<BrefHistoryProps> = ({ brefs,updateFilterBrefProduct,indexFilterProduct, products }) => {
    const classes = useStyles();
    return (
        <Card className={classes.card} >
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                   Briefs
                </Typography>
            </CardContent>
            <CardContent className={classes.underlineTop}>
                <FormControl className={classes.margin}>
                    <InputLabel htmlFor="product-customized-select">Product</InputLabel>
                    <Select
                        value={indexFilterProduct}
                        onChange={updateFilterBrefProduct}
                        input={<BootstrapInput name="product2" id="product-customized-select2" />}>
                        <MenuItem key={'-1'} value={'-1'}>None</MenuItem>
                        {products.map((product) => (
                            <MenuItem key={product.id} value={product.id}>{product.name}</MenuItem>
                        ))}

                    </Select>
                </FormControl>
            </CardContent>
            <Card className={classes.secondCard}>
            {brefs.map((bref,index )=> (
                <CardContent key={index} className={classes.lesCards}>
                        <h3>Product : {bref.product.name}</h3>
                        <h3>Title : {bref.title}</h3>
                        <p>Comment : {bref.comment}</p>
                </CardContent>
            ))}
                </Card>
            {brefs.length===0 &&  <CardContent className={classes.underlineTop}><p>No bref for this product</p></CardContent>}
        </Card>
    );
};

export default BrefHistory;
