import {createStyles, makeStyles, Theme, withStyles} from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";

export const BootstrapInput = withStyles((theme: Theme) =>
    createStyles({
        root: {
            'label + &': {
                marginTop: theme.spacing(3),
            },
        },
        input: {
            borderRadius: 4,
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
            border: '1px solid #ced4da',
            fontSize: 16,
            padding: '10px 26px 10px 12px',
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            // Use the system font instead of the default Roboto font.
            fontFamily: [
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
            '&:focus': {
                borderRadius: 4,
                borderColor: '#80bdff',
                boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
        },
    }),
)(InputBase);

export const useStyles = makeStyles((theme:Theme) =>
    createStyles({
        card: {
            minWidth: 275,
            marginLeft: '35%',

        },
        cardForm: {
            minWidth: 275,
            paddingBottom: '10%'
        },
        cardHeader:{
            borderBottom: '1px solid #f4f4f4'
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        lesCards: {
            fontSize: 12,
            borderBottom: '1px solid #f4f4f4'
        },
        secondCard: {
            overflowY: 'auto',
            maxHeight: 348,
            minWidth: 275,
        },
        margin: {
            margin: theme.spacing(1),
        },
        sendCss: {
            float: 'right',

        },
        marginBottom:{
            marginBottom: '10%'
        },
        underlineTop: {
            borderTop: '1px solid #f4f4f4'
        }
    }));