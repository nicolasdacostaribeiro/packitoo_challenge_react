import {createStyles, makeStyles, MuiThemeProvider, Theme} from "@material-ui/core";
import {theme} from "./ThemeColor";
import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        circularProgress: {
            position: 'absolute',
            left: '62%',
            top: '25%',
        },
    }),
);

export default function CircleProgress() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <MuiThemeProvider theme={theme}>
                <CircularProgress className={classes.circularProgress} />
            </MuiThemeProvider>
        </div>
    );
}