import {createMuiTheme} from "@material-ui/core";

export const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#c62828"
        },
        secondary: {
            main: '#c62828',
        },
    },
});