import React from 'react';
import { MuiThemeProvider,createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {theme} from './ThemeColor'
import logo from '../assets/Packitoo-logo.png'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        textCenter:{
            position: 'absolute',
            left: '42%',
        },
        packitoo:{
            maxHeight: 64,
        }
    }),
);

export default function DenseAppBar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <MuiThemeProvider theme={theme}>
                <AppBar position="static">
                <Toolbar variant="dense">
                    <Typography>
                    <img  className={classes.packitoo} src={logo} alt="logo-packitoo"/>
                    </Typography>
                    <Typography className={classes.textCenter} variant="h6" color="inherit">

                        Welcome to packitoo challenge !!
                    </Typography>
                </Toolbar>
            </AppBar>
            </MuiThemeProvider>
        </div>
    );
}
