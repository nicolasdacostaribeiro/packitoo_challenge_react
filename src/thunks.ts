import {ThunkAction} from "redux-thunk";
import {AppState} from "./store";
import {Action} from "redux";
import {chargBref, sendBref, addBrefOnApi, loading, recupBrefOnAPI, errorBref, SuccesBref,FilterBref} from "./store/bref/actions";
import {chargProduct, loadingProduct, recupProductOnAPI} from "./store/product/actions";
import {Bref} from "./store/bref/types";
// import {useState} from "react";

/*
------------------------------------------------------------------------------------
----------------------------------------BREF----------------------------------------
------------------------------------------------------------------------------------
*/

export const thunkChargeBref = (
): ThunkAction<void, AppState, null, Action<string>> => async dispatch => {
    dispatch(
         loading(true)
    );
    let brefs: Bref[]=await recupBrefOnAPI();
    dispatch(
         chargBref(brefs)
    );
    dispatch(
         loading(false)
    );
};

export const thunkAddBrefApi = (bref: Bref
): ThunkAction<void, AppState, null, Action<string>> => async dispatch => {
    dispatch(
        loading(true)
    );
     const succes = await addBrefOnApi(bref);
     /*
     setTimeout est utilisé ici pour tester le Loading
      */
    setTimeout(function(){
     if(succes){
         dispatch(
             sendBref(bref)
         );
         dispatch(
             loading(false)
         );
         dispatch(
             SuccesBref("the brief " + bref.title +" has been added")
         );
      }else{
         dispatch(
             errorBref("Error adding brief")
         );
     }

    }, 2000);

};

export const thunkFilterBrefApi = (valueBref: string
): ThunkAction<void, AppState, null, Action<string>> => async dispatch => {
    dispatch(
        loadingProduct(true)
    );
    dispatch(
        FilterBref(valueBref)
    );
    dispatch(
        loadingProduct(false)
    );
};
/*
------------------------------------------------------------------------------------
-------------------------------------PRODUCT----------------------------------------
------------------------------------------------------------------------------------
*/

export const thunkChargeProduct = (
): ThunkAction<void, AppState, null, Action<string>> => async dispatch => {

    dispatch(
        loadingProduct(true)
    );
    const products = await recupProductOnAPI();
    dispatch(
        chargProduct(products)
    );
    dispatch(
        loadingProduct(false)
    );
};