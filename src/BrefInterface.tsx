import * as React from "react";
import { UpdateBrefTitle, UpdateBrefComment, updateBrefProduct } from "./App";
import {Product} from "./store/product/types";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from "@material-ui/core/Typography";
import TextField from '@material-ui/core/TextField';
import {theme} from "./Style/ThemeColor";
import { MuiThemeProvider} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {BootstrapInput, useStyles} from "./Style/BrefDesign";

interface BrefInterfaceProps {
    title: string;
    comment: string;
    sendBref: (title: string,comment: string, product: Product) => void;
    updateBrefTitle: (event: UpdateBrefTitle) => void;
    updateBrefComment: (event: UpdateBrefComment) => void;
    updateBrefProduct: (event: updateBrefProduct) => void;
    products: Product[];
    indexProduct: number;
}
const BrefInterface: React.SFC<BrefInterfaceProps> = ({
                                                          title,
                                                          comment,
                                                          updateBrefTitle,
                                                          updateBrefComment,
                                                          updateBrefProduct,
                                                          sendBref,
                                                          products,indexProduct
                                                      }) => {
    const classes = useStyles();
    function keyPress(e: React.KeyboardEvent<any>) {
        if (e.key === "Enter") {
            send();
        }
    }

     function send() {
        if(Number(indexProduct)<products.length){
            sendBref(title,comment,products[Number(indexProduct)]);
        }else{
            alert('This product no exist');
        }

    }

    return (

        <Card className={classes.cardForm} >
            <CardContent className={classes.cardHeader}>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Add a Brief
                </Typography>
            </CardContent>
            <CardContent>
                <MuiThemeProvider theme={theme}>
            <div>
            <h3>Title :</h3>

                <TextField
                    id="title"
                    label="Title"
                    value={title}
                    onChange={updateBrefTitle}
                    margin="dense"
                    variant="outlined"
                    onKeyPress={keyPress}
                />

            <h3>Comment : </h3>

                    <TextField
                        id="comment"
                        label="Comment"
                        value={comment}
                        onChange={updateBrefComment}
                        margin="dense"
                        variant="outlined"
                        onKeyPress={keyPress}
                    />

            <h3>Product : </h3>
                    <FormControl className={classes.margin}>
                        <InputLabel htmlFor="age-customized-select">Product</InputLabel>
                        <Select
                            value={indexProduct}
                            onChange={updateBrefProduct}
                            input={<BootstrapInput name="product" id="product-customized-select" />}
                        >

                            {products.map((product, index) => (
                                <MenuItem key={index} value={index}>{product.name}</MenuItem>
                            ))}

                        </Select>
                    </FormControl>
                </div>
                <Button className={ classes.sendCss} onClick={send}  color={"primary"} variant="contained" >
                    Send
                </Button>
                </MuiThemeProvider>
            </CardContent>
        </Card>
    );
};

export default BrefInterface;