// import {Bref} from "../bref/types";

export interface Product {
    id: number;
    name: string;
}
export interface ProductState {
    products: Product[];
    succes: string,
    error: string,
    loading: boolean
}

// Describing the different ACTION NAMES available
export const SEND_PRODUCT = "SEND_PRODUCT";
// export const RECUP_PRODUCT = "RECUP_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";
export const CHARGER_PRODUCT = "CHARGER_PRODUCT";
export const SUCCES_PRODUCT = "SUCCES_PRODUCT";
export const ERROR_PRODUCT = "ERROR_PRODUCT";
export const LOADING_PRODUCT = "LOADING_PRODUCT";

interface SendProductAction {
    type: typeof SEND_PRODUCT;
    payload: Product;
}
interface ChargerProductAction {
    type: typeof CHARGER_PRODUCT;
    payload: Product[];
}
interface SuccesProductAction {
    type: typeof SUCCES_PRODUCT;
    payload: string;
}
interface ErrorProductAction {
    type: typeof ERROR_PRODUCT;
    payload: string;
}
interface LoadingProductAction {
    type: typeof LOADING_PRODUCT;
    payload: boolean;
}

// interface RecupProductAction {
//     type: typeof RECUP_PRODUCT;
//     payload: Bref;
// }

// interface DeleteProductAction {
//     type: typeof DELETE_PRODUCT;
//     meta: {
//         id: number;
//     };
// }

export type ProductActionTypes = SendProductAction | ChargerProductAction | SuccesProductAction | ErrorProductAction | LoadingProductAction;
