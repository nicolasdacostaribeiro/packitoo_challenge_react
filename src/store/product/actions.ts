import {CHARGER_PRODUCT, ERROR_PRODUCT, LOADING_PRODUCT, Product, SEND_PRODUCT, SUCCES_PRODUCT} from "./types";

export  function sendProduct(newProduct: Product) {
    // await addProductOnApi(newProduct);
    return {
        type: SEND_PRODUCT,
        payload: newProduct
    };
}

export function loadingProduct(load: boolean) {
    return {
        type: LOADING_PRODUCT,
        payload: load
    };
}

export function chargProduct(products: Product[]) {
    return {
        type: CHARGER_PRODUCT,
        payload: products
    }
}

export function errorProduct(error: string) {
    return {
        type: ERROR_PRODUCT,
        payload: error
    }
}
export function SuccesProduct(succes: string) {
    return {
        type: SUCCES_PRODUCT,
        payload: succes
    }
}




export async function addProductOnApi(product: Product) {
    const request= new XMLHttpRequest();
    request.open('POST','http://localhost:3000/products/',true);
    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            // Success!
            const data = JSON.parse(this.response);
            console.log(data);
        } else {
            // We reached our target server, but it returned an error
        }
    };
    request.send(JSON.stringify(product));
}

export async function searchProduct(id: number) {
    let leProduct: Product = {id: -1, name: 'Erreur Product'};
    await fetch('http://localhost:3000/products/'+id)
        .then(res => res.json())
        .then((data: Product) => {
            leProduct= data;
        });
    return leProduct;
}

export async function recupProductOnAPI() {
    let lesProducts: Product[]= [];
    await fetch('http://localhost:3000/products')
        .then(res => res.json())
        .then((data: Product[]) => {
            lesProducts= data;
        });
    return lesProducts;
}