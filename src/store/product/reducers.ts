import {
    CHARGER_PRODUCT,
    ERROR_PRODUCT,
    LOADING_PRODUCT,
    ProductActionTypes,
    ProductState,
    SEND_PRODUCT,
    SUCCES_PRODUCT
} from "./types";

const initialState: ProductState = {
    products: [],
    loading: false,
    error: '',
    succes: ''
};

export function productReducer(
    state = initialState,
    action: ProductActionTypes
): ProductState {
    switch (action.type) {
        case SEND_PRODUCT:
            return {
                products: [...state.products, action.payload],
                loading: state.loading,
                error: '',
                succes: '',
            };
        case CHARGER_PRODUCT:
            return {
                products: action.payload,
                loading: state.loading,
                error: '',
                succes: '',
            };
        case LOADING_PRODUCT:
            return {
                products: [...state.products],
                loading: action.payload,
                error: '',
                succes: '',
            };
        case ERROR_PRODUCT:
            return {
                products: [...state.products],
                loading: false,
                error: action.payload,
                succes: '',
            };
        case SUCCES_PRODUCT:
            return {
                products: [...state.products],
                loading: false,
                error: '',
                succes: action.payload,
            };
        default:
            return state;
    }
}
