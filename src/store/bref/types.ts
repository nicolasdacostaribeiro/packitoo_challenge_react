import {Product} from "../product/types";

export interface BrefJson {
    id: number
    title: string;
    comment: string;
    productId: number;
}
export interface BrefJsonPost {
    title: string;
    comment: string;
    productId: number;
}
export interface Bref {
    title: string;
    comment: string;
    product: Product;
}
export interface BrefState {
    Brefs: Bref[];
    loading: boolean;
    error: string;
    succes: string;
    filter: string;
}

// Describing the different ACTION NAMES available
export const SEND_BREF = "SEND_BREF";
export const CHARGER_BREF = "CHARGER_BREF";
export const ERROR_BREF = "ERROR_BREF";
export const LOADING_BREF = "LOADING_BREF";
export const SUCCES_BREF = "SUCCES_BREF";
export const FILTER_BREF = "FILTER_BREF";

interface SendBrefAction {
    type: typeof SEND_BREF;
    payload: Bref;
}
interface LoadinBrefAction {
    type: typeof LOADING_BREF;
    payload: boolean;
}
interface ChargerBrefAction {
    type: typeof CHARGER_BREF;
    payload: Bref[];
}
interface ErrorBrefAction {
    type: typeof ERROR_BREF;
    payload: string;
}interface SuccesBrefAction {
    type: typeof SUCCES_BREF;
    payload: string;
}
interface FilterBrefAction {
    type: typeof FILTER_BREF;
    payload: string;
}

export type BrefActionTypes = SendBrefAction | ChargerBrefAction | LoadinBrefAction | ErrorBrefAction | SuccesBrefAction | FilterBrefAction;
