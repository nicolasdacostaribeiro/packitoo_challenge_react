import {
    Bref,
    BrefJson,
    BrefJsonPost,
    CHARGER_BREF,
    ERROR_BREF,
    FILTER_BREF,
    LOADING_BREF,
    SEND_BREF,
    SUCCES_BREF
} from "./types";
// import {searchProduct} from "../product/actions";
// import {Product} from "../product/types";

export function sendBref(newBref: Bref) {
    return {
        type: SEND_BREF,
        payload: newBref
    };
}

export function loading(load: boolean) {
    return {
        type: LOADING_BREF,
        payload: load
    };
}

export function chargBref(Brefs: Bref[]) {
    return {
        type: CHARGER_BREF,
        payload: Brefs
    }
}

export function errorBref(error: string) {
    return {
        type: ERROR_BREF,
        payload: error
    }
}
export function SuccesBref(succes: string) {
    return {
        type: SUCCES_BREF,
        payload: succes
    }
}
export function FilterBref(filter: string) {
    return{
        type: FILTER_BREF,
        payload: filter
    }
}
//First version is with product recovery on the API
export async function recupBrefOnAPI() {
    let lesBrefs: BrefJson[]= [];
    let Brefs: Bref[]= [];
    // let product : Product;
    await fetch('http://localhost:3000/briefs')
        .then(res => res.json())
        .then(async (data: BrefJson[]) => {
            lesBrefs= data;
        });
    for (var i:number =0; i <lesBrefs.length;i++){
        // product = await searchProduct(lesBrefs[i].productId);
        Brefs.push({
            // comment:lesBrefs[i].comment , title: lesBrefs[i].title, product: product
            comment:lesBrefs[i].comment , title: lesBrefs[i].title, product: {id: lesBrefs[i].productId, name: ''}
        });
    }
    // console.log(Brefs);
    return Brefs;
}


 export async function addBrefOnApi(newBref: Bref): Promise<boolean> {
    try {
        var brefjson:BrefJsonPost= {title: newBref.title,comment: newBref.comment,productId: newBref.product.id};
        const response = await fetch('http://localhost:3000/briefs/', {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json",
                Accept: "application/json"
            }),
            body: JSON.stringify(brefjson)
        });
return response.ok;
} catch (ex) {
    return false;
}
}
