import {
    BrefState,
    SEND_BREF,
    LOADING_BREF,
    BrefActionTypes,
    CHARGER_BREF, ERROR_BREF, SUCCES_BREF, FILTER_BREF
} from "./types";

const initialState: BrefState = {
    Brefs: [],
    loading: false,
    error: '',
    succes: '',
    filter: 'SHOW_ALL'
};

export function brefReducer(
    state = initialState,
    action: BrefActionTypes
): BrefState {
    switch (action.type) {
        case SEND_BREF:
            return {
                Brefs: [...state.Brefs, action.payload],
                loading: state.loading,
                error: '',
                succes: '',
                filter: state.filter
            };
        case CHARGER_BREF:
            return {
                Brefs: action.payload,
                loading: state.loading,
                error: '',
                succes: '',
                filter: state.filter
            };
        case LOADING_BREF:
            return {
                Brefs: [...state.Brefs],
                loading: action.payload,
                error: '',
                succes: '',
                filter: state.filter
            };
        case ERROR_BREF:
            return {
                Brefs: [...state.Brefs],
                loading: false,
                error: action.payload,
                succes: '',
                filter: state.filter
            };
        case SUCCES_BREF:
            return {
                Brefs: [...state.Brefs],
                loading: false,
                error: '',
                succes: action.payload,
                filter: state.filter
            };
        case FILTER_BREF:
            return {
                Brefs: [...state.Brefs],
                loading: false,
                error: '',
                succes: '',
                filter: action.payload
            };
        default:
            return state;
    }
}
